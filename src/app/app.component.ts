import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { Observable, Subscriber  } from 'rxjs';
import 'rxjs/Rx';
import { DbOperationsService } from './dboperations.service';
import {  Http, Response, Headers } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'To-do in Angular 2';
  todoForm: FormGroup;
  todoList: any[] = [];
  hightlightStatus: Array<boolean> = [];

  constructor(private dbOperations: DbOperationsService){

  	
  }
  ngOnInit(){
  	this.todoForm = new FormGroup({
  		'todoTitle': new FormControl(null, Validators.required)
  	});
  	this.loadList();
  }

  addToDo(){
  	this.todoList.push(this.todoForm.get('todoTitle').value);
  	this.dbOperations.saveToDb(this.todoList).subscribe(
  			resposne => { console.log('added');}
	);
  	this.todoForm.reset();
  }

  loadList() {
		this.dbOperations.getDb()
        .subscribe(
        	(response: Response) => { 
        		const data = response.json();
        		this.todoList = data;
        	} ,
        	(error) => {console.log(error);}
        );
    };

  removeToDo(index){
  	this.todoList.splice(index, 1);
  	this.dbOperations.saveToDb(this.todoList).subscribe(
  			resposne => { console.log('deleted');}
	);
  }
}
