import { Injectable } from '@angular/core';
import {  Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';

@Injectable()
export class DbOperationsService{

	constructor(private http: Http){}

	saveToDb(toDoList: any[]){
		return this.http.put('https://ng-todo-d052e.firebaseio.com/list.json', toDoList);
	}

	getDb() {
		const headers = new Headers();
        headers.append('Access-Control-Allow-Origin', '*');
        return this.http.get('https://ng-todo-d052e.firebaseio.com/list.json', {headers: headers});
	}
}